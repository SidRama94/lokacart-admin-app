package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.MyListener;
import admin.lokacart.ict.mobile.com.adminapp.ProductActivity;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerViewAdapter;

/**
 * Created by Vishesh on 22-02-2016.
 */
public class ProductTypeFragment extends Fragment {
    View productFragmentView;
    public static RecyclerView mRecyclerView;
    public static RecyclerView.Adapter mAdapter;
    public static RecyclerView.LayoutManager mLayoutManager;
    ArrayList<String> productTypeArrayList;
    int recyclerViewIndex;
    Dialog dialog;
    JSONObject responseObject;
    ProgressDialog progressDialog;
    String newProductTypeName;
    final String LOG_TAG = "ProductTypeFrag";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        productFragmentView = inflater.inflate(R.layout.fragment_products, container, false);
        getActivity().setTitle(R.string.title_product_types);
        productTypeArrayList = new ArrayList<String>();
        recyclerViewIndex = 0;
        return productFragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = (RecyclerView) productFragmentView.findViewById(R.id.productTypeRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        new GetAdminAndProductTypeDetails(getActivity()).execute();
    }

    public void clickListener(int position) {
        Intent productIntent = new Intent(getActivity(), ProductActivity.class);
        productIntent.putExtra("product type", (String) productTypeArrayList.get(position));
        startActivity(productIntent);
    }

    public void longClickListener(final int position) {
        final EditText eEditProductTypeName, eEditProductTypeUnit;
        final Button bEditProductTypeConfirm, bEditProductTypeCancel, bEditProductTypeDelete, bEditProductTypeEdit;

        final String selectedProductType = (String) productTypeArrayList.get(position);

        if (Master.isNetworkAvailable(getActivity())) {

            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.product_type_box);
            dialog.setTitle("Modify " + selectedProductType);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            eEditProductTypeName = (EditText) dialog.findViewById(R.id.eProductTypeName);
            eEditProductTypeName.setVisibility(View.GONE);

            eEditProductTypeUnit = (EditText) dialog.findViewById(R.id.eProductTypeUnit);
            eEditProductTypeUnit.setVisibility(View.GONE);

            bEditProductTypeConfirm = (Button) dialog.findViewById(R.id.bProductTypeConfirm);
            bEditProductTypeConfirm.setVisibility(View.GONE);

            bEditProductTypeEdit = (Button) dialog.findViewById(R.id.bProductTypeEdit);
            bEditProductTypeDelete = (Button) dialog.findViewById(R.id.bProductTypeDelete);
            bEditProductTypeCancel = (Button) dialog.findViewById(R.id.bProductTypeCancel);

            bEditProductTypeCancel.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {
                                                              dialog.dismiss();
                                                          }
                                                      }
            );

            bEditProductTypeDelete.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {
                                                              JSONObject jsonObject = new JSONObject();
                                                              try {
                                                                  jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                                                  jsonObject.put("name", selectedProductType);
                                                              } catch (JSONException e) {
                                                              }
                                                              new DeleteProductTypeTask(position).execute(jsonObject);
                                                              dialog.dismiss();
                                                          }
                                                      }
            );

            bEditProductTypeEdit.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            bEditProductTypeDelete.setVisibility(View.GONE);
                                                            bEditProductTypeEdit.setVisibility(View.GONE);

                                                            eEditProductTypeName.setVisibility(View.VISIBLE);
                                                            eEditProductTypeUnit.setVisibility(View.VISIBLE);
                                                            bEditProductTypeConfirm.setVisibility(View.VISIBLE);

                                                            int indexOfBracket = selectedProductType.indexOf('(');
                                                            String pName, pUnit;
                                                            pName = selectedProductType.substring(0, indexOfBracket - 1);
                                                            pUnit = selectedProductType.substring(indexOfBracket + 2, selectedProductType.length() - 2);
                                                            eEditProductTypeName.setText(pName);
                                                            eEditProductTypeUnit.setText(pUnit);
                                                        }
                                                    }
            );

            bEditProductTypeConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (eEditProductTypeName.getText().toString().equals("")) {
                        Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_product_type, Toast.LENGTH_SHORT).show();
                    } else if (eEditProductTypeUnit.getText().toString().equals("")) {
                        Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_unit, Toast.LENGTH_SHORT).show();
                    } else {
                        String updatedProductType = eEditProductTypeName.getText().toString().trim() + " ( " + eEditProductTypeUnit.getText().toString().trim() + " )";
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("oldname", selectedProductType);
                            jsonObject.put("newname", updatedProductType);
                        } catch (JSONException e) {
                        }
                        new UpdateProductTypeTask(updatedProductType, position).execute(jsonObject);
                        dialog.dismiss();
                    }
                }
            });
            dialog.show();
        } else {
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    public void addNewProductType() {

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.product_type_box);
        dialog.setTitle(R.string.dialog_title_add_product_type);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eProductTypeName, eProductTypeUnit;
        final Button bProductTypeConfirm, bProductTypeCancel, bProductTypeDelete, bProductTypeEdit;

        eProductTypeName = (EditText) dialog.findViewById(R.id.eProductTypeName);
        eProductTypeUnit = (EditText) dialog.findViewById(R.id.eProductTypeUnit);
        bProductTypeEdit = (Button) dialog.findViewById(R.id.bProductTypeEdit);
        bProductTypeEdit.setVisibility(View.GONE);
        bProductTypeDelete = (Button) dialog.findViewById(R.id.bProductTypeDelete);
        bProductTypeDelete.setVisibility(View.GONE);

        bProductTypeConfirm = (Button) dialog.findViewById(R.id.bProductTypeConfirm);
        bProductTypeCancel = (Button) dialog.findViewById(R.id.bProductTypeCancel);

        bProductTypeCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bProductTypeConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO send the new product name
                newProductTypeName = eProductTypeName.getText().toString().trim() + " ( " +
                        eProductTypeUnit.getText().toString().trim() + " )";

                if (eProductTypeName.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_product_type, Toast.LENGTH_SHORT).show();
                } else if (eProductTypeUnit.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_unit, Toast.LENGTH_SHORT).show();
                } else if (!Master.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                } else {
                    boolean flag = true;
                    for (int i = 0; i < recyclerViewIndex; ++i) {
                        if (productTypeArrayList.get(i).equals(newProductTypeName)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("name", newProductTypeName);
                        } catch (JSONException e) {
                        }

                        new AddProductTypeTask(getActivity(), newProductTypeName).execute(jsonObject);

                    } else {
                        Toast.makeText(getActivity(), R.string.label_toast_please_enter_unique_product_type, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public class GetAdminAndProductTypeDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        Context context;
        String response;

        public GetAdminAndProductTypeDetails(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String adminDetailsAndProductTypeURL = Master.getAdminDetailsAndProductTypeURL() + "?phoneNumber=91" + AdminDetails.getMobileNumber();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(adminDetailsAndProductTypeURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            pd.dismiss();
            if (response.equals("exception")) {
                Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
            } else {
                try
                {
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray("productTypes");
                    for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                    {
                        productTypeArrayList.add(recyclerViewIndex, jsonArray.getString(recyclerViewIndex));
                    }

                    mAdapter = new RecyclerViewAdapter(productTypeArrayList, getActivity());
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.productTypeClickKey, ProductTypeFragment.this));
                }
                catch (Exception e) {

                }
            }
        }
    }

    //-----------------------Class for updating a product type-------------------------------------
    class UpdateProductTypeTask extends AsyncTask<JSONObject, String, String> {
        String response, editProductTypeName;
        int editProductTypePosition;

        UpdateProductTypeTask(String editProductTypeName, int editProductTypePosition) {
            this.editProductTypeName = editProductTypeName;
            this.editProductTypePosition = editProductTypePosition;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getEditProductTypeURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            progressDialog.dismiss();
            try {
                responseObject = new JSONObject(message);
                message = responseObject.getString("edit");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (message.equals("exception")) {
                Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
            } else if (message.equals("success")) {
                Toast.makeText(getActivity(), R.string.label_toast_Product_type_updated_successfully, Toast.LENGTH_SHORT).show();
                productTypeArrayList.set(editProductTypePosition, editProductTypeName);
                mAdapter.notifyItemChanged(editProductTypePosition);
            } else {
                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }
        }
    }
//---------------------------------------------------------------------------------------------

    //-----------------------Class for deleting a product type-------------------------------------

    /****************
     * API Format************************
     * API:
     * api/producttype/delete
     * <p/>
     * Body:
     * {
     * "orgabbr":"Test2",
     * "name":"aba (kg)"
     * }
     * Response:
     * {"message":"Product Type cannot be deleted as products of this type have been ordered"}
     * OR
     * {  "message":"Successfully deleted Product Type" }
     ***************************************************/
    class DeleteProductTypeTask extends AsyncTask<JSONObject, String, String> {
        String response;
        int productTypePosition;

        DeleteProductTypeTask(int productTypePosition) {
            this.productTypePosition = productTypePosition;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDeleteProductTypeURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            progressDialog.dismiss();
            try {
                responseObject = new JSONObject(message);
                message = responseObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!Master.isNetworkAvailable(getActivity())) {
                Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection + "", Toast.LENGTH_SHORT);
            } else if (message.equals("exception")) {
                Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
            } else if (message.equals("Successfully deleted Product Type")) {
                Toast.makeText(getActivity(), R.string.label_toast_product_type_deleted_successfully, Toast.LENGTH_SHORT).show();
                productTypeArrayList.remove(productTypePosition);
                mAdapter.notifyItemRemoved(productTypePosition);
                recyclerViewIndex--;
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------


    public class AddProductTypeTask extends AsyncTask<Object, String, String> {

        String response;
        ProgressDialog progressDialog;
        Context context;
        String newProductType;
        Master master;
        MyListener callback;

        AddProductTypeTask(Context context, String newProductType) {
            this.context = context;
            this.newProductType = newProductType;
            this.callback = (MyListener) context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Object... params) {
            master = new Master();
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(master.getAddNewProductTypeUrl(), (JSONObject) params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            progressDialog.dismiss();
            if (response.equals("exception")) {
                master.alertDialog(getActivity(), "Cannot connect to the server!", "OK");
            } else {
                try {
                    responseObject = new JSONObject(response);
                } catch (JSONException e) {
                }
                try {
                    response = responseObject.getString("upload");
                    if (response.equals("success")) {
                        Toast.makeText(getActivity(), R.string.label_toast_Product_type_added_successfully, Toast.LENGTH_SHORT).show();
                        productTypeArrayList.add(recyclerViewIndex++, newProductTypeName);
                        mAdapter.notifyItemInserted(recyclerViewIndex - 1);
                        mAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                }
            }
        }
    }
}
