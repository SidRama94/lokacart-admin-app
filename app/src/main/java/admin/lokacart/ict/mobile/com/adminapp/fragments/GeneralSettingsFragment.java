package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;
import org.json.JSONException;
import org.json.JSONObject;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by Vishesh on 29-02-2016.
 */

public class GeneralSettingsFragment extends Fragment{

    View generalSettingsFragmentView;
    ProgressDialog pd;
    String response;
    JSONObject responseObject;
    Boolean autoApprove, stockManagement;
    Button bSave;
    //ToggleButton tbAutoApprove, tbStockManagement;
    SwitchCompat sAutoApprove, sStockManagement;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        generalSettingsFragmentView = inflater.inflate(R.layout.fragment_general_settings, container, false);
        return generalSettingsFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bSave = (Button) generalSettingsFragmentView.findViewById(R.id.bGeneralSettingsSave);
        sAutoApprove = (SwitchCompat) generalSettingsFragmentView.findViewById(R.id.sAutoApprove);
        sAutoApprove.setHighlightColor(getResources().getColor(R.color.colorPrimaryDark));
        sStockManagement = (SwitchCompat) generalSettingsFragmentView.findViewById(R.id.sStockManagement);
        sStockManagement.setHighlightColor(getResources().getColor(R.color.colorPrimaryDark));
        new GetGeneralSettingsTask().execute();

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autoApprove == sAutoApprove.isChecked() && stockManagement == sStockManagement.isChecked())
                    Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_SHORT).show();
                else
                {
                    JSONObject jsonObject = new JSONObject();
                    try
                    {
                        jsonObject.put("stockManagement", sStockManagement.isChecked());
                        jsonObject.put("autoApprove", sAutoApprove.isChecked());
                        new UpdateGeneralSettingsTask().execute(jsonObject);
                    }
                    catch (JSONException e)
                    {
                    }
                }
            }
        });
    }

    public class GetGeneralSettingsTask extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getGeneralSetingsURL(AdminDetails.getAbbr(), AdminDetails.getMobileNumber()),
                    null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            pd.dismiss();
            try
            {
                responseObject = new JSONObject(response);
                if(responseObject.get("response").equals("success"))
                {
                    autoApprove = responseObject.getBoolean("autoApprove");
                    sAutoApprove.setChecked(autoApprove);
                    stockManagement = responseObject.getBoolean("stockManagement");
                    sStockManagement.setChecked(stockManagement);
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }

        }
    }

    public class UpdateGeneralSettingsTask extends AsyncTask<JSONObject, String, String>
    {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getGeneralSettingsUpdateURL(AdminDetails.getAbbr()),
                    params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            pd.dismiss();
            try
            {
                responseObject = new JSONObject(response);
                if(responseObject.get("response").equals("Successfully updated"))
                {
                    autoApprove = sAutoApprove.isChecked();
                    stockManagement = sStockManagement.isChecked();
                    Toast.makeText(getActivity(), R.string.label_toast_changes_saved_successfully, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
