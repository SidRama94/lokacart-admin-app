package admin.lokacart.ict.mobile.com.adminapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by Vishesh on 15-01-2016.
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.DataObjectHolder>
{
    static String LOG_TAG = "ProductRecyclerViewAdapter";
    ArrayList<String> productList, productQuantityList, productPriceList;
    Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView tProductName, tProductQuantity, tProductPrice, tOutOfStock;
        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            tProductName = (TextView) itemView.findViewById(R.id.tProductName);
            tProductPrice = (TextView) itemView.findViewById(R.id.tProductPrice);
            tProductQuantity = (TextView) itemView.findViewById(R.id.tProductQuantity);
            tOutOfStock = (TextView) itemView.findViewById(R.id.tOutOfStock);
        }
    }

    public ProductRecyclerViewAdapter(ArrayList<String> myDataset,ArrayList<String> quantity,ArrayList<String> price, Context context)
    {
        productList = myDataset;
        productQuantityList = quantity;
        productPriceList = price;
        this.context = context;
    }


    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card_view, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, context);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        holder.tProductName.setText(productList.get(position));
        holder.tProductPrice.setText(productPriceList.get(position));
        holder.tProductQuantity.setText(productQuantityList.get(position));
        if(productQuantityList.get(position).equals("0") || productQuantityList.get(position).equals("0.0"))
        {
            holder.tOutOfStock.setVisibility(View.VISIBLE);
        }
        else
            holder.tOutOfStock.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}
