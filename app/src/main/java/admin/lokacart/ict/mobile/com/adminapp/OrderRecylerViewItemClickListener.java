package admin.lokacart.ict.mobile.com.adminapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by root on 22/1/16.
 */
public class OrderRecylerViewItemClickListener implements RecyclerView.OnItemTouchListener{
    OrderListener callback;
    int cat;

    private GestureDetector mGestureDetector;

    public OrderRecylerViewItemClickListener(Context context,final RecyclerView recyclerView)
    {
        callback = (OrderListener)context;
        this.cat = cat;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }
            @Override
            public void onLongPress(MotionEvent e)
            {
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());

                if(childView != null)
                {
                    callback.onCardLongClickListener(recyclerView.getChildAdapterPosition(childView));
                }
            }
        });
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e)
    {
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());

        if(childView != null && mGestureDetector.onTouchEvent(e))
        {
            callback.onCardClickListener(recyclerView.getChildAdapterPosition(childView));
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }
}
