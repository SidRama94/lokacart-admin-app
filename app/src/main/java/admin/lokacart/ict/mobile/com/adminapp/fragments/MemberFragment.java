
package admin.lokacart.ict.mobile.com.adminapp.fragments;

/**
 * Created by Vishesh on 19-01-2016.
 */
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.PagerAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;

public class MemberFragment extends Fragment{

    PagerAdapter mPageAdapter;
    ViewPager viewPager;
    FragmentManager fm;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab, container, false);
        getActivity().setTitle(R.string.title_members);
        setRetainInstance(true);

        final TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_pending_request_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_existing_user_fragment)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        fm = getFragmentManager();

        mPageAdapter = new PagerAdapter(fm, tabLayout.getTabCount(),"Member");

        viewPager = (ViewPager)rootView.findViewById(R.id.pager);
        viewPager.setAdapter(mPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setCurrentItem(0);

        Bundle bundle = this.getArguments();
        try
        {
            if(bundle.getString("to").equals("existing"))
                viewPager.setCurrentItem(1);
        }
        catch (Exception e)
        {
            viewPager.setCurrentItem(0);
        }

        if(viewPager.getCurrentItem() == 1)
            DashboardActivity.viewFAB();
        else
            DashboardActivity.hideFAB();


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() == 1)
                    DashboardActivity.viewFAB();
                viewPager.setCurrentItem(tab.getPosition());
                DashboardActivity.resetBackPress();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if(tab.getPosition() == 1)
                    DashboardActivity.hideFAB();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return rootView;
    }

    public void addNewMember()
    {
        mPageAdapter.getExistingUserFragmentTab().addNewMember();
    }
}