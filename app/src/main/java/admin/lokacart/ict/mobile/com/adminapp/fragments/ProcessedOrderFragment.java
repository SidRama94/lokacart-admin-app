package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.SavedOrder;

/**
 * Created by Vishesh on 19-01-2016.
 */
public class ProcessedOrderFragment extends Fragment  {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private SwipeRefreshLayout swipeContainer;
    protected LayoutManagerType mCurrentLayoutManagerType;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    public static ArrayList<SavedOrder> processedOrderArrayList;
    View processedOrderFragmentView;
    RecyclerView mRecyclerView;
    static RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Master master;
    JSONObject responseObject;
    static int recyclerViewIndex;
    ArrayList<JSONObject> orderObjects;
    ArrayList<SavedOrder> orders;
    private int count=0;
    TextView tOrders;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        processedOrderFragmentView = inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);
        swipeContainer = (SwipeRefreshLayout) processedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetProcessedOrderDetails(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);
        return processedOrderFragmentView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        processedOrderArrayList = new ArrayList<SavedOrder>();


    }

    public ArrayList<SavedOrder> getOrders() {
        orders = new ArrayList<SavedOrder>();
        int count=0;
        if(orderObjects.size()!=0)
        {
            for(JSONObject entry : orderObjects){
                SavedOrder order = new SavedOrder(entry,count);
                orders.add(order);
                count++;
            }
        }
        else
        {


        }
        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        new GetProcessedOrderDetails(true).execute();
        mRecyclerView = (RecyclerView) processedOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        tOrders = (TextView) processedOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_processed_orders_present);
        tOrders.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }


    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {

            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void clickListener(int position)
    {
        new ViewBillAsyncTask(getActivity()).execute("" + processedOrderArrayList.get(position).getOrderId());
    }

    public class ViewBillAsyncTask extends AsyncTask<String,String,String>
    {
        Context context;
        ProgressDialog pd;

        ViewBillAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String status;
            HttpURLConnection linkConnection = null;
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String emailid = sharedPreferences.getString("emailid","test2@gmail.com");
            String password =  sharedPreferences.getString("password","password");

            try {

                URL linkurl = new URL("http://ruralict.cse.iitb.ac.in/RuralIvrs/api/"+AdminDetails.getAbbr()+"/viewbill/"+params[0]);
                linkConnection = (HttpURLConnection) linkurl.openConnection();
                String basicAuth = "Basic " + new String(Base64.encode((emailid + ":" + password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
                linkConnection.setDefaultUseCaches(false);
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "text/html");
                linkConnection.setDoInput(true);
                InputStream is = null;
                status=String.valueOf(linkConnection.getResponseCode());
                if(status.equals("200"))
                {
                    is=linkConnection.getInputStream();
                }
                else
                {
                    status="exception";
                    return  status;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                status = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
                status="exception";
                return status;
            }
            finally {
                if (linkConnection != null) {
                    linkConnection.disconnect();
                }
            }
            return status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s.equals("exception"))
            {
                Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Bill");
                WebView wv = new WebView(context);
                wv.loadData(s, "text/html", "UTF-8");
                wv.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }
                });
                alert.setView(wv);
                alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        }
    }


    public class GetProcessedOrderDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        Boolean showProgressDialog;
        String response;

        public GetProcessedOrderDetails(Boolean showProgressDialog) {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String processedOrderURL = master.getProcessedOrderURL() + "?orgabbr=" + AdminDetails.getAbbr();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(processedOrderURL, null, "GET",true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if(showProgressDialog)
                pd.dismiss();
            if (response.equals("exception"))
            {
                Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                try
                {
                    orderObjects= new ArrayList<JSONObject>();
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray("orders");
                    if(jsonArray.length() == 0)
                    {
                        tOrders.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        tOrders.setVisibility(View.GONE);
                        ++count;
                        for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                        {
                            orderObjects.add((JSONObject)jsonArray.get(recyclerViewIndex));
                        }
                        processedOrderArrayList = getOrders();
                        mAdapter = new OrderRecyclerViewAdapter(processedOrderArrayList, getActivity());
                        if(count < 2) {
                            mRecyclerView.setAdapter(mAdapter);
                            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.processedOrderKey, ProcessedOrderFragment.this));
                            mAdapter.notifyDataSetChanged();
                        }
                        else {
                            mRecyclerView.swapAdapter(mAdapter,true);
                        }
                    }

                    swipeContainer.setRefreshing(false);

                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
